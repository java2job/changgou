/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : changgou

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 19/06/2021 12:48:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_permission`;
CREATE TABLE `t_sys_permission`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `resource_id` int(0) NULL DEFAULT NULL COMMENT '资源id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `sort_num` int(0) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_permission
-- ----------------------------
INSERT INTO `t_sys_permission` VALUES (5, 7, '资源-新增', 'sys:resource:add', 1);
INSERT INTO `t_sys_permission` VALUES (6, 7, '资源-编辑', 'sys:resource:edit', 2);
INSERT INTO `t_sys_permission` VALUES (7, 7, '资源-删除', 'sys:resource:delete', 3);
INSERT INTO `t_sys_permission` VALUES (8, 7, '资源-查询', 'sys:resource:query', 4);
INSERT INTO `t_sys_permission` VALUES (9, 7, '权限-新增', 'sys:permission:add', 5);
INSERT INTO `t_sys_permission` VALUES (10, 7, '权限-编辑', 'sys:permission:edit', 6);
INSERT INTO `t_sys_permission` VALUES (11, 7, '权限-删除', 'sys:permission:delete', 7);
INSERT INTO `t_sys_permission` VALUES (12, 7, '权限-查询', 'sys:permission:query', 8);
INSERT INTO `t_sys_permission` VALUES (13, 6, '新增', 'sys:role:add', 1);
INSERT INTO `t_sys_permission` VALUES (14, 6, '编辑', 'sys:role:edit', 2);
INSERT INTO `t_sys_permission` VALUES (15, 6, '删除', 'sys:role:delete', 3);
INSERT INTO `t_sys_permission` VALUES (16, 6, '查询', 'sys:role:query', 4);
INSERT INTO `t_sys_permission` VALUES (17, 6, '分配资源权限', 'sys:role:addRP', 5);
INSERT INTO `t_sys_permission` VALUES (18, 3, '新增', 'sys:user:add', 1);
INSERT INTO `t_sys_permission` VALUES (19, 3, '编辑', 'sys:user:edit', 2);
INSERT INTO `t_sys_permission` VALUES (20, 3, '删除', 'sys:user:delete', 3);
INSERT INTO `t_sys_permission` VALUES (21, 3, '用户-查询', 'sys:user:query', 4);
INSERT INTO `t_sys_permission` VALUES (22, 3, '角色-查询', 'sys:user:role:query', 5);
INSERT INTO `t_sys_permission` VALUES (23, 3, '分配角色', 'sys:user:addUR', 6);
INSERT INTO `t_sys_permission` VALUES (24, 3, '重置密码', 'sys:user:resetPassword', 7);

-- ----------------------------
-- Table structure for t_sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_resource`;
CREATE TABLE `t_sys_resource`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `parent_id` int(0) NOT NULL COMMENT '父id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访问路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问的组件',
  `hidden_flag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否显示0：显示1隐藏',
  `sort_num` int(0) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_resource
-- ----------------------------
INSERT INTO `t_sys_resource` VALUES (2, 0, '系统管理', '/sys', 'nested', 'Layout', '0', 2);
INSERT INTO `t_sys_resource` VALUES (3, 2, '用户管理', 'user', 'user', '/sys/user/list', '0', 1);
INSERT INTO `t_sys_resource` VALUES (6, 2, '角色管理', 'role', 'password', '/sys/role/list', '0', 2);
INSERT INTO `t_sys_resource` VALUES (7, 2, '资源管理', 'resource', 'password', '/sys/resource/list', '0', 3);

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色代码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES (1, '系统管理员', '拥有所有权限');
INSERT INTO `t_sys_role` VALUES (7, '用户管理员', '只有用户管理的相关权限');
INSERT INTO `t_sys_role` VALUES (8, '资源管理员', '只有资源管理的相关权限');
INSERT INTO `t_sys_role` VALUES (9, '角色管理员', '只有角色管理的相关权限');

-- ----------------------------
-- Table structure for t_sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_permission`;
CREATE TABLE `t_sys_role_permission`  (
  `role_id` int(0) NOT NULL COMMENT '角色id',
  `permission_id` int(0) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_role_permission
-- ----------------------------
INSERT INTO `t_sys_role_permission` VALUES (1, 5);
INSERT INTO `t_sys_role_permission` VALUES (1, 6);
INSERT INTO `t_sys_role_permission` VALUES (1, 7);
INSERT INTO `t_sys_role_permission` VALUES (1, 8);
INSERT INTO `t_sys_role_permission` VALUES (1, 9);
INSERT INTO `t_sys_role_permission` VALUES (1, 10);
INSERT INTO `t_sys_role_permission` VALUES (1, 11);
INSERT INTO `t_sys_role_permission` VALUES (1, 12);
INSERT INTO `t_sys_role_permission` VALUES (1, 13);
INSERT INTO `t_sys_role_permission` VALUES (1, 14);
INSERT INTO `t_sys_role_permission` VALUES (1, 15);
INSERT INTO `t_sys_role_permission` VALUES (1, 16);
INSERT INTO `t_sys_role_permission` VALUES (1, 17);
INSERT INTO `t_sys_role_permission` VALUES (1, 18);
INSERT INTO `t_sys_role_permission` VALUES (1, 19);
INSERT INTO `t_sys_role_permission` VALUES (1, 20);
INSERT INTO `t_sys_role_permission` VALUES (1, 21);
INSERT INTO `t_sys_role_permission` VALUES (1, 22);
INSERT INTO `t_sys_role_permission` VALUES (1, 23);
INSERT INTO `t_sys_role_permission` VALUES (1, 24);
INSERT INTO `t_sys_role_permission` VALUES (7, 18);
INSERT INTO `t_sys_role_permission` VALUES (7, 19);
INSERT INTO `t_sys_role_permission` VALUES (7, 20);
INSERT INTO `t_sys_role_permission` VALUES (7, 21);
INSERT INTO `t_sys_role_permission` VALUES (7, 22);
INSERT INTO `t_sys_role_permission` VALUES (7, 24);
INSERT INTO `t_sys_role_permission` VALUES (8, 5);
INSERT INTO `t_sys_role_permission` VALUES (8, 6);
INSERT INTO `t_sys_role_permission` VALUES (8, 7);
INSERT INTO `t_sys_role_permission` VALUES (8, 8);
INSERT INTO `t_sys_role_permission` VALUES (8, 9);
INSERT INTO `t_sys_role_permission` VALUES (8, 10);
INSERT INTO `t_sys_role_permission` VALUES (8, 11);
INSERT INTO `t_sys_role_permission` VALUES (8, 12);
INSERT INTO `t_sys_role_permission` VALUES (9, 13);
INSERT INTO `t_sys_role_permission` VALUES (9, 14);
INSERT INTO `t_sys_role_permission` VALUES (9, 15);
INSERT INTO `t_sys_role_permission` VALUES (9, 16);
INSERT INTO `t_sys_role_permission` VALUES (9, 17);

-- ----------------------------
-- Table structure for t_sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_resource`;
CREATE TABLE `t_sys_role_resource`  (
  `role_id` int(0) NOT NULL COMMENT '角色id',
  `resource_id` int(0) NOT NULL COMMENT '资源id',
  PRIMARY KEY (`role_id`, `resource_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_role_resource
-- ----------------------------
INSERT INTO `t_sys_role_resource` VALUES (1, 2);
INSERT INTO `t_sys_role_resource` VALUES (1, 3);
INSERT INTO `t_sys_role_resource` VALUES (1, 6);
INSERT INTO `t_sys_role_resource` VALUES (1, 7);
INSERT INTO `t_sys_role_resource` VALUES (7, 2);
INSERT INTO `t_sys_role_resource` VALUES (7, 3);
INSERT INTO `t_sys_role_resource` VALUES (8, 2);
INSERT INTO `t_sys_role_resource` VALUES (8, 7);
INSERT INTO `t_sys_role_resource` VALUES (9, 2);
INSERT INTO `t_sys_role_resource` VALUES (9, 6);

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `status` int(0) NOT NULL COMMENT '状态：0：正常 1：停用 2：锁住',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `online_flag` int(0) NULL DEFAULT NULL COMMENT '在线标志0：在线1下线',
  `last_login_time` bigint(0) NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `create_time` bigint(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES (1, '管理员', 'admin', '$2a$10$6KX2taSdtYU2v5COkDvcTOB4p4x1jmpFWqWCC0GBmA5NlAyNPGmX2', 0, NULL, NULL, 0, 1624075386684, 1621235900344, 1624075386690);
INSERT INTO `t_sys_user` VALUES (5, '测试角色', 'test2', '$2a$10$JrVqFujAGfNFyBletVTaFurSZTJYVMeGuD1T9DybnV1tdZYjxyXVS', 0, '', '', 1, 1622962752590, 1621405328731, 1622962771700);
INSERT INTO `t_sys_user` VALUES (8, '测试资源', 'test3', '$2a$10$Vv1XMCn5xVxeU/M0DI4DnO93.jp0aClt0Xbu90m2Y2PW4YB3eCib6', 0, '', '', 1, 1622964149370, 1622192119874, 1622964164997);
INSERT INTO `t_sys_user` VALUES (10, '测试用户', 'test1', '$2a$10$eS3aOta0mBmp/A3xox8VHuIJC.6X/V93OLuSe.Xf4eTCKzocTx6q6', 0, NULL, NULL, 1, 1622967136618, 1622967082656, 1622967166061);

-- ----------------------------
-- Table structure for t_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_role`;
CREATE TABLE `t_sys_user_role`  (
  `user_id` int(0) NOT NULL COMMENT '用户id',
  `role_id` int(0) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sys_user_role
-- ----------------------------
INSERT INTO `t_sys_user_role` VALUES (1, 1);
INSERT INTO `t_sys_user_role` VALUES (5, 9);
INSERT INTO `t_sys_user_role` VALUES (8, 8);
INSERT INTO `t_sys_user_role` VALUES (10, 7);

SET FOREIGN_KEY_CHECKS = 1;
