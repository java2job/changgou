package com.xukun.changgou.service.api.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-17 18:55
 */
@Data
@Accessors(chain = true)
@Table(name = "t_sys_resource")
@ApiModel(description = "资源实体")
public class ResourceModel implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "主键")
    @ApiModelProperty(value = "资源id")
    private Integer id;

    @ApiModelProperty(value = "父id")
    @Column(name = "parent_id", columnDefinition = "父id")
    private Integer parentId;

    @ApiModelProperty(value = "资源标题")
    @Column(name = "title", columnDefinition = "标题")
    private String title;

    @ApiModelProperty(value = "资源路径")
    @Column(name = "path", columnDefinition = "路径")
    private String path;

    @ApiModelProperty(value = "资源图标")
    @Column(name = "icon", columnDefinition = "图标")
    private String icon;

    @ApiModelProperty(value = "资源组件")
    @Column(name = "component", columnDefinition = "组件")
    private String component;

    @ApiModelProperty(value = "是否隐藏",notes = "0：显示，1：隐藏")
    @Column(name = "hidden_flag", columnDefinition = "隐藏")
    private Integer hiddenFlag;

    @ApiModelProperty(value = "排序号")
    @Column(name = "sort_num", columnDefinition = "排序")
    private Integer sortNum;

    @ApiModelProperty(value = "子资源列表")
    @Transient
    private List<ResourceModel> children = new ArrayList<>();
}
