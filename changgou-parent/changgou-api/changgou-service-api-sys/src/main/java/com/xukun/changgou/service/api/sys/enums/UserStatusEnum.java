package com.xukun.changgou.service.api.sys.enums;

/**
 * @Author xukun
 * @Date 2021-05-17 14:44
 */
public enum UserStatusEnum {

    正常, 停用, 锁住;

    /**
     * 获取状态
     *
     * @param flag
     * @return <p>0：正常</p>
     * <p>1：停用</p>
     * <p>2：锁住</p>
     * <p>-1：不存在</p>
     */
    public static int getValue(UserStatusEnum flag) {
        switch (flag) {
            case 正常:
                return 0;
            case 停用:
                return 1;
            case 锁住:
                return 2;
            default:
                return -1;
        }
    }
}
