package com.xukun.changgou.service.api.sys.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author xukun
 * @Date 2021-05-19 08:57
 * 用于给角色分配权限时回显已选中的权限
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "角色权限关联实体")
public class RPVo implements Serializable {

    @ApiModelProperty(value = "角色id",required = true)
    private Integer roleId;

    @ApiModelProperty(value = "关联的权限id数据",required = true)
    private String[] rpIds;
}
