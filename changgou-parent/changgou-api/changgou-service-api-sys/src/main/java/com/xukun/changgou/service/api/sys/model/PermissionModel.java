package com.xukun.changgou.service.api.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @Author xukun
 * @Date 2021-05-17 19:10
 */
@Data
@Accessors(chain = true)
@Table(name = "t_sys_permission")
@ApiModel(description = "权限实体")
public class PermissionModel implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "主键")
    @ApiModelProperty(value = "权限id")
    private Integer id;

    @ApiModelProperty(value = "所属资源id")
    @Column(name = "resource_id", columnDefinition = "资源id")
    private Integer resourceId;

    @ApiModelProperty(value = "权限名称")
    @Column(name = "name", columnDefinition = "名称")
    private String name;

    @ApiModelProperty(value = "权限标识")
    @Column(name = "code", columnDefinition = "权限标识")
    private String code;

    @ApiModelProperty(value = "排序号")
    @Column(name = "sort_num", columnDefinition = "排序")
    private Integer sortNum;

    @ApiModelProperty(value = "所属资源名称")
    @Transient
    private String resourceName;
}
