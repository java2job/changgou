package com.xukun.changgou.service.api.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author xukun
 * @Date 2021-05-17 18:18
 */
@Data
@Accessors(chain = true)
@Table(name = "t_sys_role")
@ApiModel(description = "角色实体")
public class RoleModel implements Serializable {

    @Id
    @Column(name = "id", columnDefinition = "主键")
    @ApiModelProperty(value = "角色id")
    private Integer id;

    @ApiModelProperty(value = "角色名称")
    @Column(name = "name", columnDefinition = "名称")
    private String name;

    @ApiModelProperty(value = "角色描述")
    @Column(name = "description", columnDefinition = "描述")
    private String description;

}

