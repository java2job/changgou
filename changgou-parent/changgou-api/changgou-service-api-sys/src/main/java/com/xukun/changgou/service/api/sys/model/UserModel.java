package com.xukun.changgou.service.api.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author xukun
 * @Date 2021-05-17 13:33
 */
@Data
@Accessors(chain = true)
@Table(name = "t_sys_user")
@ApiModel(description = "用户实体")
public class UserModel implements Serializable {
    @Id
    @Column(name = "id", columnDefinition = "主键")
    @ApiModelProperty(value = "用户id")
    private Integer id;

    @ApiModelProperty(value = "用户名称")
    @Column(name = "name", columnDefinition = "名称")
    private String name;

    @ApiModelProperty(value = "用户账号")
    @Column(name = "code", columnDefinition = "登录账号")
    private String code;

    @ApiModelProperty(value = "登录密码")
    @Column(name = "password", columnDefinition = "登录密码")
    private String password;

    @ApiModelProperty(value = "用户状态",notes = "0:正常，1：停用，2：锁住")
    @Column(name = "status", columnDefinition = "状态")
    private Integer status;

    @ApiModelProperty(value = "电话")
    @Column(name = "phone", columnDefinition = "电话")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    @Column(name = "email", columnDefinition = "邮箱")
    private String email;

    @ApiModelProperty(value = "在线标志",notes = "0：在线，1：下线")
    @Column(name = "online_flag", columnDefinition = "在线标志")
    private Integer onlineFlag;

    @ApiModelProperty(value = "最后一次登录时间")
    @Column(name = "last_login_time", columnDefinition = "最后一次登录时间")
    private Long lastLoginTime;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time", columnDefinition = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time", columnDefinition = "更新时间")
    private Long updateTime;

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", onlineFlag=" + onlineFlag +
                ", lastLoginTime=" + lastLoginTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
