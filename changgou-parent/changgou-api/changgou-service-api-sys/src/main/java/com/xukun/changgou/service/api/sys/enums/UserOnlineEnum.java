package com.xukun.changgou.service.api.sys.enums;

/**
 * @Author xukun
 * @Date 2021-05-17 14:44
 */
public enum UserOnlineEnum {

    在线, 下线;

    /**
     * 获取状态
     *
     * @param flag
     * @return <p>0：在线</p>
     * <p>1：下线</p>
     * <p>-1：不存在</p>
     */
    public static int getValue(UserOnlineEnum flag) {
        switch (flag) {
            case 在线:
                return 0;
            case 下线:
                return 1;
            default:
                return -1;
        }
    }
}
