package com.xukun.changgou.service.api.sys.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-18 19:14
 * 用于给角色分配权限时使用
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "资源树实体")
public class ResourceTreeVo implements Serializable {

    @ApiModelProperty(value = "资源id")
    private String id;

    @ApiModelProperty(value = "资源名称")
    private String name;

    @ApiModelProperty(value = "子资源")
    private List<ResourceTreeVo> children = new ArrayList<>();

}
