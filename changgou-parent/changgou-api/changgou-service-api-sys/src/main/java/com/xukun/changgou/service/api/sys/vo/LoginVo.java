package com.xukun.changgou.service.api.sys.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author xukun
 * @Date 2021-05-17 14:54
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "登录实体")
public class LoginVo implements Serializable {

    @ApiModelProperty(value = "账号", required = true)
    private String username;
    @ApiModelProperty(value = "密码", required = true)
    private String password;

}
