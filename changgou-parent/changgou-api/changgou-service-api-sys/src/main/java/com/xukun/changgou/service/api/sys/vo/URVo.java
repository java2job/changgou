package com.xukun.changgou.service.api.sys.vo;

import com.xukun.changgou.service.api.sys.model.RoleModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-19 08:57
 * 用于给用户分配角色时回显
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "用户角色关联实体")
public class URVo implements Serializable {

    @ApiModelProperty(value = "用户id", required = true)
    private Integer userId;

    @ApiModelProperty(value = "关联的角色列表", required = true)
    private List<RoleModel> roleList;
}
