package com.xukun.changgou.auth.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-06-02 15:42
 * @Description
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BaseUserDetails implements UserDetails {


    private String username;
    private String password;
    private Boolean enabled;
    private List<? extends GrantedAuthority> authorities;

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}
