package com.xukun.changgou.auth.feign.sys;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.service.api.sys.model.UserModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xukun
 * @Date 2021-06-03 16:53
 * @Description
 */
@FeignClient(value = "changgou-sys", fallbackFactory = SysUserFallbackFactory.class)
@RequestMapping("/inner/sys/user")
public interface SysUserFeign {

    //根据账号获取用户信息
    @GetMapping("/queryUserByCode/{code}")
    ResponseData queryUserByCode(@PathVariable("code") String code);

    //根据用户id获取权限列表
    @GetMapping("/queryPermissionListByUser/{userId}")
    ResponseData queryPermissionListByUser(@PathVariable("userId") Integer userId);

    //更新用户信息
    @PostMapping("/update")
    ResponseData update(@RequestBody UserModel user);

}
