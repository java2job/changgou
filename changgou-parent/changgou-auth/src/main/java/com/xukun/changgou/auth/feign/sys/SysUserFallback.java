package com.xukun.changgou.auth.feign.sys;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.model.UserModel;

/**
 * @Author xukun
 * @Date 2021-06-03 16:57
 * @Description
 */
public class SysUserFallback implements SysUserFeign {

    private Throwable throwable;

    public SysUserFallback(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public ResponseData queryUserByCode(String code) {
        return ResponseUtil.fail("changgou-sys服务异常：" + throwable);
    }

    @Override
    public ResponseData queryPermissionListByUser(Integer userId) {
        return ResponseUtil.fail("changgou-sys服务异常：" + throwable);
    }

    @Override
    public ResponseData update(UserModel user) {
        return ResponseUtil.fail("changgou-sys服务异常：" + throwable);
    }
}
