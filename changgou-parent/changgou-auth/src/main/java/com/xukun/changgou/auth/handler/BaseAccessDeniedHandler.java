package com.xukun.changgou.auth.handler;

import com.xukun.changgou.common.response.ResponseEnum;
import com.xukun.changgou.common.utils.HttpUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xukun
 * @Date 2021-06-03 16:34
 * @Description 处理登录的用户访问无权限资源时的异常
 */
@Component
public class BaseAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        HttpUtil.doReturn(response, false, ResponseEnum.NO_ACCESS);
    }
}