package com.xukun.changgou.auth.controller;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xukun
 * @Date 2021-06-04 17:59
 * @Description
 */
@Api(tags = {"系统安全控制接口"})
@RestController
@RequestMapping("/auth")
public class AuthController {

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public ResponseData login(@RequestBody LoginVo loginVo) {
        return ResponseUtil.success();
    }

    @ApiOperation(value = "登出")
    @GetMapping("/logout")
    public ResponseData logout() {
        return ResponseUtil.success();
    }
}
