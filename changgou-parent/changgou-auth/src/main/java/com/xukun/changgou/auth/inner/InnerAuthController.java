package com.xukun.changgou.auth.inner;

import com.xukun.changgou.auth.feign.sys.SysUserFeign;
import com.xukun.changgou.common.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author xukun
 * @Date 2021-06-06 13:40
 * @Description
 */
@RestController
@RequestMapping("/inner/auth")
public class InnerAuthController {

    @Autowired
    private SysUserFeign userFeign;

    @GetMapping("/getCurUser")
    public ResponseData getCurUser() {
        String code = SecurityContextHolder.getContext().getAuthentication().getName();
        return userFeign.queryUserByCode(code);
    }
}
