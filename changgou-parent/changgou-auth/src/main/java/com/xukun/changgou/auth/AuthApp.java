package com.xukun.changgou.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author xukun
 * @Date 2021-05-31 16:06
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
//@MapperScan("com.xukun.changgou.auth.dao")
@ComponentScan("com.xukun.changgou")
@JsonComponent//jacksonObject转换
@EnableDiscoveryClient
@EnableFeignClients
public class AuthApp {

    public static final Logger log = LoggerFactory.getLogger(AuthApp.class);

    public static void main(String[] args) {
        SpringApplication.run(AuthApp.class, args);
        log.debug("Auth模块启动成功");
    }
}
