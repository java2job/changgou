package com.xukun.changgou.auth.config.interceptor;

import com.xukun.changgou.common.constants.CommonConstant;
import com.xukun.changgou.common.response.ResponseEnum;
import com.xukun.changgou.common.response.exception.BaseException;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author xukun
 * @Date 2021-06-04 15:27
 * @Description 自定义安全拦截器，处理请求的接口的的权限
 */
public class BaseSecurityInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //在网关处已获取过权限，此处直接获取即可，如果获取不到，说明没有从网关处进行请求，进行拦截
        String authority = request.getHeader(CommonConstant.AUTH_HEADER);
        if (ObjectUtils.isEmpty(authority)) {
            throw new BaseException(ResponseEnum.DENY_ACCESS);
        }
        //由于该服务用于登录登出，不做权限处理问题，如有需要，参考sys模块
        return true;
    }
}
