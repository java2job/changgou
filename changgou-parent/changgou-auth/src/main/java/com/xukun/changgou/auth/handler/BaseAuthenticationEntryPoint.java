package com.xukun.changgou.auth.handler;

import com.xukun.changgou.common.response.ResponseEnum;
import com.xukun.changgou.common.utils.HttpUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xukun
 * @Date 2021-06-03 16:33
 * @Description 处理用户未登录进行访问时的异常（即未登录，或者登录状态过期失效）
 */
@Component
public class BaseAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        HttpUtil.doReturn(response, false, ResponseEnum.NOT_LOGIN);
    }
}