package com.xukun.changgou.service.sys.controller;

import com.xukun.changgou.common.annotation.BaseAccess;
import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.model.PermissionModel;
import com.xukun.changgou.service.sys.service.PermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xukun
 * @Date 2021-05-17 13:43
 */
@Api(tags = {"权限管理接口"})
@RestController
@RequestMapping("/sys/permission")
public class PermissionController {

    @Autowired
    private PermissionService service;

    @BaseAccess(value = {"sys:permission:add"})
    @ApiOperation(value = "新增权限")
    @PostMapping("/insert")
    public ResponseData insert(@RequestBody PermissionModel permission) {
        int result = service.insert(permission);
        if (result != 1) {
            return ResponseUtil.fail("添加失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:permission:edit"})
    @ApiOperation(value = "更新权限")
    @PostMapping("/update")
    public ResponseData update(@RequestBody PermissionModel permission) {
        int result = service.update(permission);
        if (result != 1) {
            return ResponseUtil.fail("更新失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:permission:delete"})
    @ApiOperation(value = "删除权限")
    @DeleteMapping("/delete/{id}")
    public ResponseData delete(@ApiParam("权限id") @PathVariable("id") Integer id) {
        int result = service.deleteById(id);
        if (result != 1) {
            return ResponseUtil.fail("删除失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:permission:query"})
    @ApiOperation(value = "查询权限列表")
    @GetMapping("/queryPageList")
    public ResponseData queryOne(@ApiParam("分页号") @RequestParam("pageNum") Integer pageNum, @ApiParam("分页数") @RequestParam("pageSize") Integer pageSize, @ApiParam("关键字") String key) {
        return ResponseUtil.success(service.queryPageList(pageNum, pageSize, key));
    }
}
