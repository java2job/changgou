package com.xukun.changgou.service.sys.controller;

import com.xukun.changgou.common.annotation.BaseAccess;
import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.model.RoleModel;
import com.xukun.changgou.service.api.sys.vo.RPVo;
import com.xukun.changgou.service.sys.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xukun
 * @Date 2021-05-17 13:43
 */
@Api(tags = {"角色管理接口"})
@RestController
@RequestMapping("/sys/role")
public class RoleController {

    @Autowired
    private RoleService service;

    @BaseAccess(value = {"sys:role:add"})
    @ApiOperation(value = "新增角色")
    @PostMapping("/insert")
    public ResponseData insert(@RequestBody RoleModel role) {
        int result = service.insert(role);
        if (result != 1) {
            return ResponseUtil.fail("添加失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:role:edit"})
    @ApiOperation(value = "更新角色")
    @PostMapping("/update")
    public ResponseData update(@RequestBody RoleModel role) {
        int result = service.update(role);
        if (result != 1) {
            return ResponseUtil.fail("更新失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:role:delete"})
    @ApiOperation(value = "删除角色")
    @DeleteMapping("/delete/{id}")
    public ResponseData delete(@ApiParam("角色id") @PathVariable("id") Integer id) {
        int result = service.deleteById(id);
        if (result != 1) {
            return ResponseUtil.fail("删除失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:role:query"})
    @ApiOperation(value = "查询角色列表")
    @GetMapping("/queryList")
    private ResponseData queryPageList() {
        return ResponseUtil.success(service.queryList());
    }

    @BaseAccess(value = {"sys:role:addRP"})
    @ApiOperation(value = "根据角色查询拥有的资源权限id列表")
    @GetMapping("/queryHasResourceByRole/{id}")
    private ResponseData queryHasResourceByRole(@ApiParam("角色id") @PathVariable("id") Integer id) {
        return ResponseUtil.success(service.queryHasResourceByRole(id));
    }

    @BaseAccess(value = {"sys:role:addRP"})
    @ApiOperation(value = "为角色分配资源权限")
    @PostMapping("/insertRP")
    private ResponseData insertRP(@RequestBody RPVo rpVo) {
        service.insertRP(rpVo);
        return ResponseUtil.success();
    }

}
