package com.xukun.changgou.service.sys.feign.auth;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Author xukun
 * @Date 2021-06-06 13:39
 * @Description
 */
@Component
public class AuthFallbackFactory implements FallbackFactory<AuthFallback> {

    @Override
    public AuthFallback create(Throwable throwable) {
        return new AuthFallback(throwable);
    }

}