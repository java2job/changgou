package com.xukun.changgou.service.sys.inner;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.model.UserModel;
import com.xukun.changgou.service.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-06-03 17:00
 * @Description
 */

@RestController
@RequestMapping("/inner/sys/user")
public class InnerUserController {

    @Autowired
    private UserService userService;


    //根据账号获取用户信息
    @GetMapping("/queryUserByCode/{code}")
    public ResponseData queryUserByCode(@PathVariable("code") String code) {
        UserModel user = userService.queryUserByCode(code);
        return ResponseUtil.success(user);
    }

    //根据用户id获取权限列表
    @GetMapping("/queryPermissionListByUser/{userId}")
    public ResponseData queryPermissionListByUser(@PathVariable("userId") Integer userId) {
        List<String> list = userService.queryPermissionListByUser(userId);
        return ResponseUtil.success(list);
    }

    //更新用户信息
    @PostMapping("/update")
    public ResponseData update(@RequestBody UserModel user) {
        int result = userService.update(user);
        if (result != 1) {
            return ResponseUtil.fail("更新失败");
        }
        return ResponseUtil.success();
    }
}
