package com.xukun.changgou.service.sys.config;

import com.alibaba.fastjson.JSON;
import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.service.api.sys.model.UserModel;
import com.xukun.changgou.service.sys.feign.auth.AuthFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author xukun
 * @Date 2021-06-06 13:38
 * @Description 当前登录用户
 */
@Component
public class CurUser {

    @Autowired
    private AuthFeign authFeign;

    /**
     * 获取当前登录用户信息
     */
    public UserModel getCurUser() {
        ResponseData userData = authFeign.getCurUser();
        UserModel userModel = JSON.parseObject(JSON.toJSONString(userData.getData()), UserModel.class);
        return userModel;
    }

}
