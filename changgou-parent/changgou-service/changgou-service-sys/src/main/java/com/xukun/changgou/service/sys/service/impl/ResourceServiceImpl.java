package com.xukun.changgou.service.sys.service.impl;

import com.xukun.changgou.common.response.exception.BaseException;
import com.xukun.changgou.service.api.sys.model.PermissionModel;
import com.xukun.changgou.service.api.sys.model.ResourceModel;
import com.xukun.changgou.service.api.sys.vo.ResourceTreeVo;
import com.xukun.changgou.service.sys.dao.PermissionDao;
import com.xukun.changgou.service.sys.dao.ResourceDao;
import com.xukun.changgou.service.sys.service.ResourceService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-17 13:43
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceDao dao;

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public int insert(ResourceModel record) {
        ResourceModel one = dao.selectOne(new ResourceModel().setPath(record.getPath()));
        if (!ObjectUtils.isEmpty(one)) {
            throw new BaseException("路径已存在");
        }
        if (ObjectUtils.isEmpty(record.getParentId())) {
            record.setParentId(0);
        }
        if (ObjectUtils.isEmpty(record.getComponent())) {
            record.setComponent("Layout");
        }
        int result = dao.insertUseGeneratedKeys(record);
        return result;
    }

    @Override
    public int update(ResourceModel record) {
        ResourceModel one = dao.selectOne(new ResourceModel().setPath(record.getPath()));
        if (ObjectUtils.isNotEmpty(one) && one.getId() != record.getId()) {
            throw new BaseException("路径已存在");
        }
        if (ObjectUtils.isEmpty(record.getParentId())) {
            record.setParentId(0);
        }
        if (ObjectUtils.isEmpty(record.getComponent())) {
            record.setComponent("Layout");
        }
        return dao.updateByPrimaryKeySelective(record);
    }

    @Transactional
    @Override
    public int deleteById(Integer id) {

        List<ResourceModel> children = dao.queryListByParent(id);
        if (ObjectUtils.isNotEmpty(children)) {
            throw new BaseException("请先删除子资源");
        }
        List<PermissionModel> permissionModels = permissionDao.queryListByResource(id);
        if (ObjectUtils.isNotEmpty(permissionModels)) {
            for (PermissionModel permissionModel : permissionModels) {
                permissionDao.deleteRoleByPermission(permissionModel.getId());
            }
        }
        permissionDao.delete(new PermissionModel().setResourceId(id));
        //删除与角色的关联关系
        dao.deleteRoleByResource(id);
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public ResourceModel queryOneById(Integer id) {
        return dao.selectByPrimaryKey(id);
    }

    @Override
    public List<ResourceModel> queryTreeList() {
        //parentId=0
        List<ResourceModel> list = dao.queryListByParent(0);
        if (ObjectUtils.isNotEmpty(list)) {
            for (ResourceModel resource : list) {
                List<ResourceModel> children = dao.queryListByParent(resource.getId());
                queryTreeList1(children);
                resource.setChildren(children);
            }
        }
        return list;
    }

    private List<ResourceModel> queryTreeList1(List<ResourceModel> list) {
        if (ObjectUtils.isNotEmpty(list)) {
            for (ResourceModel resource : list) {
                List<ResourceModel> children = dao.queryListByParent(resource.getId());
                resource.setChildren(queryTreeList1(children));
            }
        }
        return list;
    }

    @Override
    public List<ResourceTreeVo> queryTreeAll() {

        List<ResourceTreeVo> tree = new ArrayList<>();

        List<ResourceModel> list = dao.queryListByParent(0);
        if (ObjectUtils.isNotEmpty(list)) {
            for (ResourceModel resource : list) {
                ResourceTreeVo treeVo = new ResourceTreeVo().setId(String.valueOf(resource.getId())).setName(resource.getTitle());
                tree.add(treeVo);
                List<ResourceModel> children = dao.queryListByParent(resource.getId());
                if (ObjectUtils.isEmpty(children)) {
                    //找到对应的权限转为资源
                    List<PermissionModel> permissionModels = permissionDao.queryListByResource(resource.getId());
                    List<ResourceTreeVo> treeVoChildren = new ArrayList<>();
                    if (ObjectUtils.isNotEmpty(permissionModels)) {
                        for (PermissionModel permissionModel : permissionModels) {
                            ResourceTreeVo treeVo1 = new ResourceTreeVo().setId(resource.getId() + "P" + permissionModel.getId()).setName(permissionModel.getName());
                            treeVoChildren.add(treeVo1);
                        }
                    }
                    treeVo.setChildren(treeVoChildren);
                } else {
                    queryTreeAll1(treeVo, children);
                }
            }
        }
        return tree;
    }

    private void queryTreeAll1(ResourceTreeVo treeVo, List<ResourceModel> list) {
        if (ObjectUtils.isNotEmpty(list)) {
            for (ResourceModel resource : list) {
                ResourceTreeVo treeVo1 = new ResourceTreeVo().setId(treeVo.getId() + "R" + resource.getId()).setName(resource.getTitle());
                treeVo.getChildren().add(treeVo1);
                List<ResourceModel> children = dao.queryListByParent(resource.getId());
                if (ObjectUtils.isEmpty(children)) {
                    //找到对应的权限转为资源
                    List<PermissionModel> permissionModels = permissionDao.queryListByResource(resource.getId());
                    List<ResourceTreeVo> treeVoChildren = new ArrayList<>();
                    if (ObjectUtils.isNotEmpty(permissionModels)) {
                        for (PermissionModel permissionModel : permissionModels) {
                            ResourceTreeVo treeVo2 = new ResourceTreeVo().setId(treeVo1.getId() + "P" + permissionModel.getId()).setName(permissionModel.getName());
                            treeVoChildren.add(treeVo2);
                        }
                    }
                    treeVo1.setChildren(treeVoChildren);
                } else {
                    queryTreeAll1(treeVo1, children);
                }
            }
        }
    }
}
