package com.xukun.changgou.service.sys.service;

import com.xukun.changgou.common.base.BaseService;
import com.xukun.changgou.service.api.sys.model.ResourceModel;
import com.xukun.changgou.service.api.sys.model.RoleModel;
import com.xukun.changgou.service.api.sys.vo.ResourceTreeVo;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-17 13:41
 */
public interface ResourceService extends BaseService<ResourceModel, Integer> {

    List<ResourceModel> queryTreeList();

    List<ResourceTreeVo> queryTreeAll();
}
