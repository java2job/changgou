package com.xukun.changgou.service.sys.dao;

import com.xukun.changgou.common.base.BaseDao;
import com.xukun.changgou.service.api.sys.model.ResourceModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-17 19:12
 */
@Repository
public interface ResourceDao extends BaseDao<ResourceModel> {

    List<ResourceModel> queryListByParent(@Param("parentId") Integer parentId);

    void deleteRoleByResource(@Param("resourceId")Integer resourceId);

    List<ResourceModel> queryListByParentAndUser(@Param("parentId") Integer parentId,@Param("userId") Integer userId);
}
