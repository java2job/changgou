package com.xukun.changgou.service.sys.service;

import com.xukun.changgou.common.base.BaseService;
import com.xukun.changgou.service.api.sys.model.RoleModel;
import com.xukun.changgou.service.api.sys.vo.RPVo;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-17 13:41
 */
public interface RoleService extends BaseService<RoleModel, Integer> {

    List<RoleModel> queryList();

    /**
     * 分局角色查询拥有的资源权限列表
     * @param id 角色id
     * @return 资源和权限id组成的数组，资源之间用'R',资源与权限之间用‘P’
     */
    List<String> queryHasResourceByRole(Integer id);

    /**
     * 为角色分配资源权限
     */
    void insertRP(RPVo rpVo);
}
