package com.xukun.changgou.service.sys.dao;

import com.xukun.changgou.common.base.BaseDao;
import com.xukun.changgou.service.api.sys.model.PermissionModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-05-17 19:12
 */
@Repository
public interface PermissionDao extends BaseDao<PermissionModel> {

    List<PermissionModel> queryPageList(@Param("key") String key);

    List<PermissionModel> queryListByResource(@Param("resourceId") Integer resourceId);

    void deleteRoleByPermission(@Param("permissionId") Integer permissionId);

}
