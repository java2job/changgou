package com.xukun.changgou.service.sys.service;

import com.github.pagehelper.PageInfo;
import com.xukun.changgou.common.base.BaseService;
import com.xukun.changgou.service.api.sys.model.ResourceModel;
import com.xukun.changgou.service.api.sys.model.RoleModel;
import com.xukun.changgou.service.api.sys.model.UserModel;
import com.xukun.changgou.service.api.sys.vo.LoginVo;
import com.xukun.changgou.service.api.sys.vo.URVo;

import java.util.List;
import java.util.Set;

/**
 * @Author xukun
 * @Date 2021-05-17 13:41
 */
public interface UserService extends BaseService<UserModel, Integer> {

    /**
     * 查询当前用户的分页列表
     */
    PageInfo<UserModel> queryPageList(Integer pageNum, Integer pageSize, String key);

    /**
     * 根据用户查询该用户拥有的角色列表
     */
    List<RoleModel> queryRoleListByUser(Integer userId);

    /**
     * 给用户分配角色
     */
    void insertUR(URVo urVo);

    /**
     * 查询当前用户拥有的资源列表（菜单）
     */
    List<ResourceModel> queryResourceTreeByCurrentUser();

    /**
     * 查询当前用户拥有的权限列表
     */
    List<String> queryPermissionByCurrentUser();

    /**
     * 根据code获取用户信息
     * @param code 账号
     */
    UserModel queryUserByCode(String code);

    /**
     * 根据用户id查询用户拥有的权限列表
     */
    List<String> queryPermissionListByUser(Integer userId);
}
