package com.xukun.changgou.service.sys.controller;

import com.xukun.changgou.common.annotation.BaseAccess;
import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.model.ResourceModel;
import com.xukun.changgou.service.sys.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xukun
 * @Date 2021-05-17 13:43
 */
@Api(tags = {"资源管理接口"})
@RestController
@RequestMapping("/sys/resource")
public class ResourceController {

    @Autowired
    private ResourceService service;

    @BaseAccess(value = {"sys:resource:add"})
    @ApiOperation(value = "新增资源")
    @PostMapping("/insert")
    public ResponseData insert(@RequestBody ResourceModel resource) {
        int result = service.insert(resource);
        if (result != 1) {
            return ResponseUtil.fail("添加失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:resource:edit"})
    @ApiOperation(value = "更新资源")
    @PostMapping("/update")
    public ResponseData update(@RequestBody ResourceModel resource) {
        int result = service.update(resource);
        if (result != 1) {
            return ResponseUtil.fail("更新失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:resource:delete"})
    @ApiOperation(value = "删除资源")
    @DeleteMapping("/delete/{id}")
    public ResponseData delete(@ApiParam("资源id") @PathVariable("id") Integer id) {
        int result = service.deleteById(id);
        if (result != 1) {
            return ResponseUtil.fail("删除失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:resource:query"})
    @ApiOperation(value = "查询资源树(不包含权限)")
    @GetMapping("/queryTreeList")
    public ResponseData queryTreeList() {
        return ResponseUtil.success(service.queryTreeList());
    }

    @BaseAccess(value = {"sys:role:addRP"})
    @ApiOperation(value = "查询包含权限的资源树")
    @GetMapping("/queryTreeAll")
    public ResponseData queryTreeAll() {
        return ResponseUtil.success(service.queryTreeAll());
    }
}
