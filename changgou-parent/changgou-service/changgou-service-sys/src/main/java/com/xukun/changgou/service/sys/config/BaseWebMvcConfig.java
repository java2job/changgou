package com.xukun.changgou.service.sys.config;

import com.xukun.changgou.service.sys.config.interceptor.BaseSecurityInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author xukun
 * @Date 2021-06-04 15:23
 * @Description 配置拦截器
 */
@Configuration
public class BaseWebMvcConfig implements WebMvcConfigurer {

    @Bean
    public BaseSecurityInterceptor baseSecurityInterceptor() {
        return new BaseSecurityInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加自定义的拦截器
        InterceptorRegistration addInterceptor = registry.addInterceptor(baseSecurityInterceptor());
        String[] excludePath = {
                "/swagger-resources",
                "/v2/api-docs",
                "/inner/**"
        };
        //拦截所有请求
        addInterceptor.addPathPatterns("/**").excludePathPatterns(excludePath);
    }
}
