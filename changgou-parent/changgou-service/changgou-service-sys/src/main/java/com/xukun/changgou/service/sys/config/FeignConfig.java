package com.xukun.changgou.service.sys.config;

import com.xukun.changgou.common.constants.CommonConstant;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author xukun
 * @Date 2021-06-06 13:11
 * @Description 处理Feign调用时添加X-Token请求头，以便在Auth模块可以认证获取当前用户
 */
@Configuration
public class FeignConfig implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        requestTemplate.header(CommonConstant.TOKEN_HEADER, request.getHeader(CommonConstant.TOKEN_HEADER));
    }
}
