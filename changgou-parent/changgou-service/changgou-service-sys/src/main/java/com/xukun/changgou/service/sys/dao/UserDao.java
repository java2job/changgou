package com.xukun.changgou.service.sys.dao;

import com.xukun.changgou.common.base.BaseDao;
import com.xukun.changgou.service.api.sys.model.RoleModel;
import com.xukun.changgou.service.api.sys.model.UserModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author xukun
 * @Date 2021-04-03 20:32
 */
@Repository
public interface UserDao extends BaseDao<UserModel> {

    List<UserModel> queryList(@Param("key") String key);

    List<RoleModel> queryRoleListByUser(@Param("userId")Integer userId);

    void deleteRoleByUser(@Param("userId") Integer userId);

    void insertUserRole(@Param("userId") Integer userId, @Param("roleList") List<RoleModel> roleList);

    List<String> queryPermissionListByUser(@Param("userId") Integer userId);

    List<String> queryPermissionListByCode(@Param("code") String code);
}
