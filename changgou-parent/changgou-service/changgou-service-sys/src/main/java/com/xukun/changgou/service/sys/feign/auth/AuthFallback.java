package com.xukun.changgou.service.sys.feign.auth;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;

/**
 * @Author xukun
 * @Date 2021-06-06 13:39
 * @Description
 */
public class AuthFallback implements AuthFeign {

    private Throwable throwable;

    public AuthFallback(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public ResponseData getCurUser() {
        return ResponseUtil.fail("changgou-auth服务异常：" + throwable);
    }
}
