package com.xukun.changgou.service.sys.service;

import com.github.pagehelper.PageInfo;
import com.xukun.changgou.common.base.BaseService;
import com.xukun.changgou.service.api.sys.model.PermissionModel;
import com.xukun.changgou.service.api.sys.model.ResourceModel;

/**
 * @Author xukun
 * @Date 2021-05-17 13:41
 */
public interface PermissionService extends BaseService<PermissionModel, Integer> {

    /**
     * 分页查询权限列表
     * @return
     */
    PageInfo<PermissionModel> queryPageList(Integer pageNum, Integer pageSize, String key);
}
