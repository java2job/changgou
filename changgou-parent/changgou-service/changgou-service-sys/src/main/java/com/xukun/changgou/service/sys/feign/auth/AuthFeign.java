package com.xukun.changgou.service.sys.feign.auth;

import com.xukun.changgou.common.response.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author xukun
 * @Date 2021-06-06 13:39
 * @Description
 */
@FeignClient(value = "changgou-auth", fallbackFactory = AuthFallbackFactory.class)
@RequestMapping("/inner/auth")
public interface AuthFeign {

    /**
     * 获取当前登录用户code
     *
     * @return
     */
    @GetMapping("/getCurUser")
    ResponseData getCurUser();
}
