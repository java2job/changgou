package com.xukun.changgou.service.sys.controller;

import com.xukun.changgou.common.annotation.BaseAccess;
import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.service.api.sys.model.UserModel;
import com.xukun.changgou.service.api.sys.vo.URVo;
import com.xukun.changgou.service.sys.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @Author xukun
 * @Date 2021-05-17 13:43
 */
@Api(tags = {"用户管理接口"})
@RestController
@RequestMapping("/sys/user")
public class UserController {

    @Autowired
    private UserService service;

    @BaseAccess(value = {"sys:user:add"})
    @ApiOperation(value = "新增用户")
    @PostMapping("/insert")
    public ResponseData insert(@RequestBody UserModel user) {
        int result = service.insert(user);
        if (ObjectUtils.isEmpty(result)) {
            return ResponseUtil.fail("添加失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:user:edit"})
    @ApiOperation(value = "更新用户")
    @PostMapping("/update")
    public ResponseData update(@RequestBody UserModel user) {
        int result = service.update(user);
        if (result != 1) {
            return ResponseUtil.fail("更新失败");
        }
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:user:delete"})
    @ApiOperation(value = "删除用户")
    @DeleteMapping("/delete/{id}")
    public ResponseData delete(@ApiParam(value = "用户id") @PathVariable("id") Integer id) {
        int result = service.deleteById(id);
        if (result != 1) {
            return ResponseUtil.fail("删除失败");
        }
        return ResponseUtil.success();
    }

//    @ApiOperation(value = "查询用户")
//    @GetMapping("/queryOne/{id}")
//    public ResponseData queryOne(@PathVariable("id") Integer id) {
//        return ResponseUtil.success(service.queryOneById(id));
//    }

    @BaseAccess(value = {"sys:user:query"})
    @ApiOperation(value = "查询用户分页")
    @GetMapping("/queryPageList")
    private ResponseData queryPageList(@ApiParam("分页号") @RequestParam("pageNum") Integer pageNum, @ApiParam("分页数") @RequestParam("pageSize") Integer pageSize, @ApiParam("关键字") String key) {
        return ResponseUtil.success(service.queryPageList(pageNum, pageSize, key));
    }

    @BaseAccess(value = {"sys:user:role:query"})
    @ApiOperation(value = "查询用户拥有的角色列表")
    @GetMapping("/queryRoleListByUser/{userId}")
    public ResponseData queryRoleListByUser(@ApiParam(value = "用户id") @PathVariable("userId") Integer userId) {
        return ResponseUtil.success(service.queryRoleListByUser(userId));
    }

    @BaseAccess(value = {"sys:user:addUR"})
    @ApiOperation(value = "为用户分配角色")
    @PostMapping("/insertUserRole")
    public ResponseData insertUserRole(@RequestBody URVo urVo) {
        service.insertUR(urVo);
        return ResponseUtil.success();
    }

    @BaseAccess(value = {"sys:user:resetPassword"})
    @ApiOperation(value = "重置密码")
    @PostMapping("/resetPassword")
    public ResponseData resetPassword(@RequestBody UserModel userModel) {
        int result = service.update(userModel);
        if (result != 1) {
            return ResponseUtil.fail("更新失败");
        }
        return ResponseUtil.success();
    }


    @ApiOperation(value = "查询当前用户拥有的菜单列表")
    @GetMapping("/queryResourceTreeByCurrentUser")
    public ResponseData queryResourceTreeByCurrentUser() {
        return ResponseUtil.success(service.queryResourceTreeByCurrentUser());
    }

    @ApiOperation(value = "查询当前用户拥有的权限列表")
    @GetMapping("/queryPermissionByCurrentUser")
    public ResponseData queryPermissionByCurrentUser() {
        return ResponseUtil.success(service.queryPermissionByCurrentUser());
    }

    @ApiOperation("模拟获取用户信息")
    @GetMapping("/getInfo")
    public ResponseData getInfo() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "admin");
        map.put("avatar", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        return ResponseUtil.success(map);
    }
}
