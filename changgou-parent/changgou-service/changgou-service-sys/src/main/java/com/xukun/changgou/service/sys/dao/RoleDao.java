package com.xukun.changgou.service.sys.dao;

import com.xukun.changgou.common.base.BaseDao;
import com.xukun.changgou.service.api.sys.model.PermissionModel;
import com.xukun.changgou.service.api.sys.model.ResourceModel;
import com.xukun.changgou.service.api.sys.model.RoleModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author xukun
 * @Date 2021-04-03 20:32
 */
@Repository
public interface RoleDao extends BaseDao<RoleModel> {

    List<RoleModel> queryList();

    List<ResourceModel> queryHasResourceByRoleAndParent(@Param("roleId") Integer roleId, @Param("reourceParent") Integer reourceParent);

    List<PermissionModel> queryHasPermissionByRoleAndResource(@Param("roleId") Integer roleId, @Param("reource") Integer reource);

    int deleteResourceByRole(Integer roleId);

    int deletePermissionByRole(Integer roleId);

    void insertRoleResource(@Param("roleId") Integer roleId, @Param("resourceIds") Set<Integer> resourceIds);

    void insertRolePermission(@Param("roleId") Integer roleId, @Param("permissionIds") Set<Integer> permissionIds);
}
