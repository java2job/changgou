package com.xukun.changgou.common.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @Author xukun
 * @Date 2021-04-06 14:11
 * 通用的Dao,已封装好基本的数据库操作方法
 */
public interface BaseDao<T> extends Mapper<T>, MySqlMapper<T> {

}
