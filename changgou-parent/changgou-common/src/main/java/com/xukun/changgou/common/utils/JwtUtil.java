package com.xukun.changgou.common.utils;

import com.xukun.changgou.common.constants.CommonConstant;
import com.xukun.changgou.common.response.ResponseEnum;
import com.xukun.changgou.common.response.exception.BaseException;
import io.jsonwebtoken.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;

/**
 * @Author xukun
 * @Date 2021-04-03 15:22
 * Jwt工具类
 */
public class JwtUtil {

    /**
     * 生成token
     *
     * @param subject   主体对象，如用户名
     * @param ttlMillis 过期时间，默认为一小时
     */
    public static String generateToken(String subject, Long ttlMillis) {
        //指定算法
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //当前系统时间
        long nowMillis = System.currentTimeMillis();
        //令牌签发时间
        Date now = new Date(nowMillis);

        //如果令牌有效期为null，则默认设置有效期1小时
        if (ttlMillis == null) {
            ttlMillis = CommonConstant.TOKEN_TTL;
        }

        //令牌过期时间设置
        long expMillis = nowMillis + ttlMillis;
        Date expDate = new Date(expMillis);

        //生成秘钥
        SecretKey secretKey = generateKey();

        //封装Jwt令牌信息
        JwtBuilder builder = Jwts.builder()
                .setSubject(subject)          // 主题  可以是JSON数据
                .setIssuer(CommonConstant.TOKEN_ISSUER)          // 签发者
                .setIssuedAt(now)             // 签发时间
                .signWith(signatureAlgorithm, secretKey) // 签名算法以及密匙
                .setExpiration(expDate);      // 设置过期时间
        return builder.compact();
    }

    /**
     * 生成token
     *
     * @param subject 主体对象，如用户名
     */
    public static String generateToken(String subject) {
        //指定算法
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //当前系统时间
        long nowMillis = System.currentTimeMillis();
        //令牌签发时间
        Date now = new Date(nowMillis);

        //如果令牌有效期为null，则默认设置有效期1小时

        //令牌过期时间设置
        Date expDate = new Date(nowMillis + CommonConstant.TOKEN_TTL);

        //生成秘钥
        SecretKey secretKey = generateKey();

        //封装Jwt令牌信息
        JwtBuilder builder = Jwts.builder()
                .setSubject(subject)          // 主题  可以是JSON数据
                .setIssuer(CommonConstant.TOKEN_ISSUER)          // 签发者
                .setIssuedAt(now)             // 签发时间
                .signWith(signatureAlgorithm, secretKey) // 签名算法以及密匙
                .setExpiration(expDate);      // 设置过期时间
        return builder.compact();
    }

    /**
     * 生成加密 secretKey
     */
    private static SecretKey generateKey() {
        byte[] encodedKey = Base64.getEncoder().encode(CommonConstant.TOKEN_KEY.getBytes());
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }


    /**
     * 解析令牌数据
     *
     * @param token token字符串
     * @return
     * @throws Exception
     */
    public static Claims parseJWT(String token) {
        try {
            SecretKey secretKey = generateKey();
            return Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e) {
            throw new BaseException(ResponseEnum.EXPIRE_TOKEN);
        } catch (MalformedJwtException e) {
            throw new BaseException(ResponseEnum.INVALID_TOKEN);
        } catch (SignatureException e) {
            throw new BaseException(ResponseEnum.NOT_MATCH_TOKEN);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 解析令牌数据
     *
     * @param token token字符串
     */
    public static String getSubject(String token) {
        try {
            SecretKey secretKey = generateKey();
            return Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody().getSubject();
        } catch (ExpiredJwtException e) {
            throw new BaseException(ResponseEnum.EXPIRE_TOKEN);
        } catch (MalformedJwtException e) {
            throw new BaseException(ResponseEnum.INVALID_TOKEN);
        } catch (SignatureException e) {
            throw new BaseException(ResponseEnum.NOT_MATCH_TOKEN);
        } catch (Exception e) {
            throw e;
        }
    }
}
