package com.xukun.changgou.common.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author xukun
 * @Date 2021-04-03 14:05
 * 响应结果实体类
 */
@Data
@ApiModel(description = "响应实体")
public class ResponseData<T> implements Serializable {

    @ApiModelProperty(value = "响应码")
    private Integer code;//返回码
    @ApiModelProperty(value = "响应信息")
    private String message;//返回消息
    @ApiModelProperty("响应数据")
    private T data;//返回数据

    public ResponseData(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResponseData(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseData() {
    }

}
