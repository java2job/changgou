package com.xukun.changgou.common.response.exception;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseEnum;
import com.xukun.changgou.common.response.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author xukun
 * @Date 2021-04-04 21:24
 */
@RestControllerAdvice
public class BaseExceptionController {

    private Logger logger = LoggerFactory.getLogger(BaseExceptionController.class);

    @ExceptionHandler(BaseException.class)
    public ResponseData error(BaseException e) {
        return ResponseUtil.fail(e.getCode(), e.getMsg());
    }

    @ExceptionHandler(Exception.class)
    public ResponseData error(Exception e) {
        e.printStackTrace();
        return ResponseUtil.fail(ResponseEnum.UNKNOW);
    }

}
