package com.xukun.changgou.common.annotation;

import java.lang.annotation.*;

/**
 * @Author xukun
 * @Date 2021-06-04 15:32
 * @Description 该注解用于权限控制
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface BaseAccess {

    //权限值
    String[] value();

    //所有的权限都满足，默认为true
    boolean requireAll() default true;
}
