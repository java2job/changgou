package com.xukun.changgou.common.response.exception;

import com.xukun.changgou.common.response.ResponseEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Author xukun
 * @Date 2021-04-04 21:23
 */
@Getter
@NoArgsConstructor
public class BaseException extends RuntimeException {

    private Integer code;

    private String msg;

    public BaseException(String msg) {
        this.code = ResponseEnum.FAIL.getCode();
        this.msg = msg;
    }

    public BaseException(ResponseEnum responseEnum) {
        this.code = responseEnum.getCode();
        this.msg = responseEnum.getMessage();
    }

    @Override
    public String toString() {
        return "BaseException " + msg;
    }
}
