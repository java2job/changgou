package com.xukun.changgou.common.response;

/**
 * @Author xukun
 * @Date 2021-04-04 20:57
 * Result工具类
 */
public class ResponseUtil {

    public static ResponseData success() {
        ResponseEnum success = ResponseEnum.SUCCESS;
        return new ResponseData(success.getCode(), success.getMessage());
    }

    public static <T> ResponseData success(String message, T data) {
        ResponseEnum success = ResponseEnum.SUCCESS;
        return new ResponseData(success.getCode(), message, data);
    }

    public static <T> ResponseData success(T  data) {
        ResponseEnum success = ResponseEnum.SUCCESS;
        return new ResponseData(success.getCode(), success.getMessage(), data);
    }

    public static ResponseData success(ResponseEnum responseEnum) {
        return new ResponseData(responseEnum.getCode(), responseEnum.getMessage());
    }

    public static <T> ResponseData success(ResponseEnum responseEnum, T data) {
        return new ResponseData(responseEnum.getCode(), responseEnum.getMessage(), data);
    }

    public static ResponseData fail() {
        ResponseEnum fail = ResponseEnum.FAIL;
        return new ResponseData(fail.getCode(), fail.getMessage());
    }

    public static ResponseData fail(String message) {
        ResponseEnum fail = ResponseEnum.FAIL;
        return new ResponseData(fail.getCode(), message);
    }

    public static ResponseData fail(Integer code, String message) {
        return new ResponseData(code, message);
    }

    public static <T> ResponseData fail(Integer code, String message, T data) {
        return new ResponseData(code, message, data);
    }

    public static ResponseData fail(ResponseEnum responseEnum) {
        return new ResponseData(responseEnum.getCode(), responseEnum.getMessage());
    }

    public static <T> ResponseData fail(ResponseEnum responseEnum, T data) {
        return new ResponseData(responseEnum.getCode(), responseEnum.getMessage(), data);
    }
}
