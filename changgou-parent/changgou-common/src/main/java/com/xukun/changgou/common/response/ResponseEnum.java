package com.xukun.changgou.common.response;

/**
 * @Author xukun
 * @Date 2021-05-27 14:13
 */
public enum ResponseEnum {

    SUCCESS(0, "请求成功"),
    FAIL(-1, "请求失败"),
    UNKNOW(-2, "未知错误"),
    DENY_ACCESS(1,"拒绝直接访问服务"),
    DENY_ACCESS_INNER(2,"禁止访问内部接口"),


    NO_ACCESS(40001, "访问受限，请联系管理员添加权限"),

    NOT_LOGIN(50001, "未登录"),
    EXPIRE_TOKEN(50002, "token过期"),
    NOT_MATCH_TOKEN(50003, "token不匹配"),
    INVALID_TOKEN(50004, "token不存在或无效");

    private int code;
    private String message;

    ResponseEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
