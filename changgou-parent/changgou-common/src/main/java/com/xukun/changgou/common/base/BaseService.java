package com.xukun.changgou.common.base;

/**
 * @Author xukun
 * @Date 2021-04-06 15:11
 * 通用Service,实现其中的方法编写具体的业务
 */
public interface BaseService<T, E> {

    /**
     * 添加一条记录
     *
     * @return 添加成功返回1
     */
    int insert(T record);

    /**
     * 修改一条记录
     *
     * @return 修改成功返回1
     */
    int update(T record);

    /**
     * 根据id删除一条记录
     *
     * @return 删除成功返回1
     */
    int deleteById(E id);

    /**
     * 根据id查询记录
     *
     * @return
     */
    T queryOneById(E id);
}
