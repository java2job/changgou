package com.xukun.changgou.common.constants;

/**
 * @Author xukun
 * @Date 2021-05-28 14:25
 */
public interface CommonConstant {

    /**
     * jwt生成token的有效期 默认60 * 60 *1000  一个小时
     */

    long TOKEN_TTL = 3600000L;

    /**
     * jwt生成token加密的秘钥
     */
    String TOKEN_KEY = "xukun";

    /**
     * 用来解析request的请求
     */
    String TOKEN_HEADER = "X-Token";

    /**
     * jwt生成token的签发人
     */
    String TOKEN_ISSUER = "xukun";

    /**
     * redis中存储的用户权限后缀
     */
    String REDIS_USER_AUTH_SUFFIX = "_auth";

    /**
     * redis中存储的token黑名单
     */
    String REDIS_TOKEN_BLACK_LIST = "token_black_list";

    /**
     * 网关处获取权限存放在request的请求头中传递下去
     */
    String AUTH_HEADER = "X-Auth";
}
