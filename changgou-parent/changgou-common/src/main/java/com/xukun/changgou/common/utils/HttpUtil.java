package com.xukun.changgou.common.utils;

import com.alibaba.fastjson.JSON;
import com.xukun.changgou.common.response.ResponseEnum;
import com.xukun.changgou.common.response.ResponseUtil;
import com.xukun.changgou.common.response.exception.BaseException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xukun
 * @Date 2021-05-27 11:29
 */
public class HttpUtil {

    /**
     * @param response HttpServletResponse响应对象
     * @param flag     请求是否成功
     * @param message  响应信息
     */
    public static void doReturn(HttpServletResponse response, boolean flag, String message) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        try {
            if (flag) {
                response.getWriter().print(JSON.toJSONString(ResponseUtil.success(message, null)));
            } else {
                response.getWriter().print(JSON.toJSONString(ResponseUtil.fail(message)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param response     HttpServletResponse响应对象
     * @param flag         请求是否成功
     * @param responseEnum 响应枚举(code,message)
     */
    public static void doReturn(HttpServletResponse response, boolean flag, ResponseEnum responseEnum) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        try {
            if (flag) {
                response.getWriter().print(JSON.toJSONString(ResponseUtil.success(responseEnum)));
            } else {
                response.getWriter().print(JSON.toJSONString(ResponseUtil.fail(responseEnum)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param response     HttpServletResponse响应对象
     * @param flag         请求是否成功
     * @param responseEnum 响应枚举(code,message)
     * @param data         响应数据
     */
    public static void doReturn(HttpServletResponse response, boolean flag, ResponseEnum responseEnum, Object data) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        try {
            if (flag) {
                response.getWriter().print(JSON.toJSONString(ResponseUtil.success(responseEnum, data)));
            } else {
                response.getWriter().print(JSON.toJSONString(ResponseUtil.fail(responseEnum, data)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param response  HttpServletResponse响应对象
     * @param exception 自定义异常对象
     */
    public static void doReturn(HttpServletResponse response, BaseException exception) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        try {
            response.getWriter().print(JSON.toJSONString(ResponseUtil.fail(exception.getCode(), exception.getMsg())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
