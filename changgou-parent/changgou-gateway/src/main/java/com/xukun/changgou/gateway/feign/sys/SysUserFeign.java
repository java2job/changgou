package com.xukun.changgou.gateway.feign.sys;

import com.xukun.changgou.common.response.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xukun
 * @Date 2021-06-03 16:53
 * @Description
 */
@FeignClient(value = "changgou-sys", fallbackFactory = SysUserFallbackFactory.class)
@RequestMapping("/inner/sys/user")
public interface SysUserFeign {

    //根据用户id获取权限列表
    @GetMapping("/queryPermissionListByUser/{userId}")
    ResponseData queryPermissionListByUser(@PathVariable("userId") Integer userId);

}
