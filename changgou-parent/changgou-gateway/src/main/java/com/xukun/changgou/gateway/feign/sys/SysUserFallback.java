package com.xukun.changgou.gateway.feign.sys;

import com.xukun.changgou.common.response.ResponseData;
import com.xukun.changgou.common.response.ResponseUtil;

/**
 * @Author xukun
 * @Date 2021-06-03 16:57
 * @Description
 */
public class SysUserFallback implements SysUserFeign {

    private Throwable throwable;

    public SysUserFallback(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public ResponseData queryPermissionListByUser(Integer userId) {
        return ResponseUtil.fail("changgou-sys服务异常：" + throwable);
    }

}
