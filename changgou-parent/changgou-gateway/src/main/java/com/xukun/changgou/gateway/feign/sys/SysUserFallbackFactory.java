package com.xukun.changgou.gateway.feign.sys;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Author xukun
 * @Date 2021-06-03 16:56
 * @Description
 */
@Component
public class SysUserFallbackFactory implements FallbackFactory<SysUserFallback> {
    @Override
    public SysUserFallback create(Throwable throwable) {
        return new SysUserFallback(throwable);
    }
}
