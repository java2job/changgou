package com.xukun.changgou.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author xukun
 * @Date 2021-06-04 09:31
 * @Description
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan("com.xukun.changgou")
@EnableDiscoveryClient
@EnableFeignClients
public class GatewayApp {

    public static final Logger log = LoggerFactory.getLogger(GatewayApp.class);

    public static void main(String[] args) {
        SpringApplication.run(GatewayApp.class, args);
        log.debug("Gateway模块启动成功");
    }
}
